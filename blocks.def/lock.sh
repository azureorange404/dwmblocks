#!/bin/sh

# RED='\033[0;31m'
# NC='\033[0m' # No Color

enabled="󰌾 "
# disabled="${RED}󱙱 ${NC}"
disabled="󱙱 "
LOCK_STATE="$(cat "$HOME"/.local/state/lockstate)"

if [ "$LOCK_STATE" = "enabled" ]; then
    ICON="$enabled"
    state="on"
elif [ "$LOCK_STATE" = "disabled" ]; then
    ICON="$disabled"
    state="off"
else
    ICON="? "
    state="???"
fi

printf "$ICON%s" "$state"
