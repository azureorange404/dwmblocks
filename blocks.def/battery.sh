#!/bin/bash

read -r capacity </sys/class/power_supply/BAT?/capacity
read -r state </sys/class/power_supply/BAT?/status

if [ "$state" = "Charging" ]; then
    ICON="󱐋 "
elif [ "$capacity" -lt 15 ]; then
    ICON=" "
elif [ "$capacity" -lt 25 ]; then
    ICON=" "
elif [ "$capacity" -lt 50 ]; then
    ICON=" "
elif [ "$capacity" -lt 75 ]; then
    ICON=" "
else
    ICON=" "
fi

printf "$ICON%s%%" "$capacity"
