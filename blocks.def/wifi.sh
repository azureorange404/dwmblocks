#!/bin/sh

if [ "$(cat /sys/class/net/enp4s0f1/carrier)" -eq 1 ]
then
    ICON="󰈀 "
    NET="eth"
    printf "$ICON %s" "$NET"
    exit 0
fi

NET="$(iwgetid | cut -d'"' -f2)"

STRENGTH=$(awk 'NR==3 {print substr($3, 1, length($3)-1)}' < /proc/net/wireless)
[ "$STRENGTH" = "" ] && STRENGTH=0

if [ "$STRENGTH" -gt 75 ]; then
    ICON="󰤨 "
elif [ "$STRENGTH" -gt 50 ]; then
    ICON="󰤥 "
elif [ "$STRENGTH" -gt 25 ]; then
    ICON="󰤢 "
elif [ "$STRENGTH" -gt 0 ]; then
    ICON="󰤟 "
else
    ICON="󰤮 "
    NET="---"
fi

printf "$ICON%s" "$NET"
