#!/bin/sh

STATE_FILE=/tmp/makropadx/state

[ -f $STATE_FILE ] && STATE=$(cat /tmp/makropadx/state) || STATE="unknown"

case $STATE in
    num)
        ICON=" "
        ;;
    off)
        ICON=" "
        ;;
    tag)
        ICON=" "
        ;;
    win)
        ICON=" "
        ;;
    *)
        ICON=" "
        ;;
esac

printf "%s" "$ICON"

