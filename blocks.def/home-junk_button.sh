#!/bin/sh

script="$(basename "$0")"
path="$(realpath "$0" | sed -e "s/blocks[.def]*\/$script//")"
signal="$(grep "$script" "$path"config.def.h | sed 's/ \+/ /g' | cut -d' ' -f 6 | cut -d'}' -f1)"

DMENU() {
    dmenu -c -bw 1 -nb '#222222' -nf '#bbbbbb' -sb '#255957' -sf '#eeeeee' -l 20 -p "$1"
}

ls -A "$HOME" > /tmp/home.txt
NEWFILES=$(diff /tmp/home.txt "$HOME"/.local/share/home.txt | grep '^<' | sed 's/^<\ //')
[ -n "$NEWFILES" ] && COUNT=$(printf "%b\n" "$NEWFILES" | wc -l) || exit 0

case "$1" in
    1) 
        sigdwmblocks "$signal"
        ;;
    3) 
        CHOICE=$(printf "%b" "list\npurge\npersist" | DMENU "Home directory:")

        case $CHOICE in
            list)
                st -t float -g 60x$((COUNT + 6)) \
                    -e bash -c "printf 'Junk Files:\n\n%s\n\n' '$NEWFILES' && read"
                ;;
            purge)
                FILE=$(printf "%b" "all\n$NEWFILES" | DMENU "delete:") || exit 1
                YESNO=$(printf "%b" "No\nYes" | DMENU "Delete $FILE?")
                case $YESNO in
                    Yes)
                        case $FILE in
                            all)
                                for i in $NEWFILES
                                do
                                    rm -rf "${HOME:?}/$i"
                                done
                                ;;
                            *)
                                rm -rf "${HOME:?}/$FILE"
                                ;;
                        esac
                        rm /tmp/home.txt
                        ;;
                    No) 
                        rm /tmp/home.txt
                        exit 0
                        ;;
                    *) 
                        rm /tmp/home.txt
                        exit 1
                        ;;
                esac
                ;;
            persist)
                FILE=$(printf "%b" "all\n$NEWFILES" | DMENU "persist:") || exit 1
                YESNO=$(printf "%b" "No\nYes" | DMENU "Persist $FILE?")
                case $YESNO in 
                    Yes)
                        case $FILE in 
                            all)
                                printf "%b" "$NEWFILES" >> "$HOME/.local/share/home.txt"
                                sort "$HOME/.local/share/home.txt" > /tmp/newhome.txt
                                mv /tmp/newhome.txt "$HOME/.local/share/home.txt"
                                ;;
                            *)
                                echo "$FILE" >> "$HOME/.local/share/home.txt"
                                sort "$HOME/.local/share/home.txt" > /tmp/newhome.txt
                                mv /tmp/newhome.txt "$HOME/.local/share/home.txt"
                                ;;
                        esac
                        rm /tmp/home.txt
                        ;;
                    No)
                        rm /tmp/home.txt
                        exit 0
                        ;;
                    *) 
                        rm /tmp/home.txt
                        exit 1
                        ;;
                esac
                ;;
            *)
                rm /tmp/home.txt
                exit 1
                ;;
        esac
        sigdwmblocks "$signal"
        ;;
esac
