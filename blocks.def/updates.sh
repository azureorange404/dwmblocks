#!/bin/sh

ICON="󰇚 "

UPDATECOUNT=$(checkupdates | wc -l)

printf "$ICON%s" "$UPDATECOUNT"
