#!/bin/sh

script="$(basename "$0")"
path="$(realpath "$0" | sed -e "s/blocks[.def]*\/$script//")"
signal="$(grep "$script" "$path"config.def.h | sed 's/ \+/ /g' | cut -d' ' -f 6 | cut -d'}' -f1)"

case "$1" in
    1) 
        sigdwmblocks "$signal"
        ;;
    3) 
        CHOICE=$(printf "%b" "Yes\nNo" | dmenu -c -bw 1 -nb "#222222" -nf "#bbbbbb" -sb "#255957" -sf "#eeeeee" -l 2 -p "Update?")
        case $CHOICE in
            Yes)
                st -t float -g 150x50 -e bash -c "sudo pacman -Syu";;
            No) exit 0;;
        esac
        sigdwmblocks "$signal"
        ;;
esac
