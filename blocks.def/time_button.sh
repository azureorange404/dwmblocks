#!/bin/sh

script="$(basename "$0")"
path="$(realpath "$0" | sed -e "s/blocks[.def]*\/$script//")"
signal="$(grep "$script" "$path"config.def.h | sed 's/ \+/ /g' | cut -d' ' -f 6 | cut -d'}' -f1)"

case $1 in
    1)
        sigdwmblocks "$signal" 1
        sleep 5
        sigdwmblocks "$signal" 0
        exit 0
        ;;
    3)
        CHOICE=$(printf "%b" "timer\nstopwatch" | dmenu -c -bw 1 -nb "#222222" -nf "#bbbbbb" -sb "#255957" -sf "#eeeeee" -l 2 -p "Choose:")

        case $CHOICE in
            timer)
                TIME=$(printf | dmenu -c -bw 1 -nb "#222222" -nf "#bbbbbb" -sb "#255957" -sf "#eeeeee" -l 0 -p "Enter time in seconds:")
                if [ -z "$TIME" ]; then
                    exit 1
                fi
                if [ "$TIME" -lt 60 ]; then
                    st -t float -g 25x9 -e bash -c "sh $HOME/.local/source/scripts/timer.sh $TIME"
                elif [ "$TIME" -lt 3600 ]; then
                    st -t float -g 55x9 -e bash -c "sh $HOME/.local/source/scripts/timer.sh $TIME"
                else
                    st -t float -g 80x9 -e bash -c "sh $HOME/.local/source/scripts/timer.sh $TIME"
                fi
                ;;
            stopwatch)
                st -t float -g 55x9 -e bash -c "sh $HOME/.local/source/scripts/stopwatch.sh"
                ;;
            *)
                exit 1
                ;;
        esac
        ;;
esac
