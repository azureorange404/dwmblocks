#!/bin/sh

xdotool key super+Escape

if xdotool getwindowgeometry "$(xdotool search --class dwm)" | grep "Position: 0,0" >/dev/null 2>&1; then
    echo "visible"
else
    echo "hidden"
fi

