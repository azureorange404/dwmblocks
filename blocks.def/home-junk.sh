#!/bin/sh

rmdir "$HOME/Desktop" 2>/dev/null

ICON="󰈸 "

NORMCOUNT=$(cat "$HOME/.local/share/home.txt" | wc -l)
FILECOUNT=$(ls -A "$HOME" | wc -l)

JUNKCOUNT=$(( FILECOUNT - NORMCOUNT ))

printf "$ICON%s" "$JUNKCOUNT"
