#!/bin/sh

script="$(basename "$0")"
path="$(realpath "$0" | sed -e "s/blocks[.def]*\/$script//")"
signal="$(grep "$script" "$path"config.def.h | sed 's/ \+/ /g' | cut -d' ' -f 6 | cut -d'}' -f1)"

LOCK_FILE="$HOME/.local/state/lockstate"
LOCK_STATE="$(cat "$LOCK_FILE")"

case "$1" in
    1)
        if [ "$LOCK_STATE" = "enabled" ]; then
            echo "disabled" > "$LOCK_FILE"
        else
            echo "enabled" > "$LOCK_FILE"
        fi
        sigdwmblocks "$signal"
        dunstify -r 2500 -t 750 -u low "locking" "$(cat "$LOCK_FILE")"
        ;;
    3)
        dm-logout
    ;;
esac
