#!/bin/sh

script="$(basename "$0")"
path="$(realpath "$0" | sed -e "s/blocks[.def]*\/$script//")"
signal="$(grep "$script" "$path"config.def.h | sed 's/ \+/ /g' | cut -d' ' -f 6 | cut -d'}' -f1)"

# DMENU="dmenu -c -bw 1 -nb #222222 -nf #bbbbbb -sb #255957 -sf #eeeeee -bw 1 -i -l 2 -p"

case "$1" in
    1) 
        sigdwmblocks "$signal"
        ;;
    3) 
        # if [ "$(printf "%s\n" "No" "Yes" | ${DMENU} "restart network?")" = "Yes" ]; then
        #     systemctl restart systemd-networkd.service
        # fi
        dm-net
        sigdwmblocks "$signal"
        ;;
esac
