#!/bin/sh

ICON=" "

case "$1" in
    1) printf "$ICON%s %s %s\n" "$(date +KW%V)" "$(date +%d.%m.%y)" "$(date +%H:%M)";;
    *) printf "$ICON%s\n" "$(date +%H:%M)";;
esac

