#!/bin/sh

script="$(basename "$0")"
path="$(realpath "$0" | sed -e "s/blocks[.def]*\/$script//")"
signal="$(grep "$script" "$path"config.def.h | sed 's/ \+/ /g' | cut -d' ' -f 6 | cut -d'}' -f1)"

case "$1" in
    1) 
        brightnessctl s 1
        sigdwmblocks "$signal"
        ;;
    3) 
        brightnessctl s 75%
        sigdwmblocks "$signal"
        ;;
esac
