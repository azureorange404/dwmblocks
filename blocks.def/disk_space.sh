#!/bin/bash

ICON="󰉉 "

output=""
spacer=""
devices="$(lsblk -o NAME,FSUSE% | grep "[0-9]%" | sed -e 's/^.*sd//' -e 's/^.*nvme0n1p/p/' -e 's/\s\+/ /')"

while IFS= read -r line; do
    percentage="$(echo "$line" | cut -d' ' -f2 | sed -e 's/%//')"
    
    case "$1" in
        1)
            device="$(echo "$devices" | grep "$percentage%" | sed -e 's/^\([a-z]*[0-9]*\)/\1:/' -e 's/ //g')"
            output="$device $output"
            ;;
        *) 
            device="$(echo "$devices" | grep "$percentage%" | sed -e 's/^\([a-z]*[0-9]*\)/\1:/' -e 's/ //g')"
            if [ "$percentage" -gt 80 ]; then
                output="$device$spacer$output"
            fi
            spacer=" / "
            ;;
    esac
done <<< "$devices"

printf "$ICON %s\n" "$output"
