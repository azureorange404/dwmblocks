#!/bin/bash

CPU="$(awk '{u=$2+$4; t=$2+$4+$5; if (NR==1){u1=u; t1=t;} else print ($2+$4-u1) * 100 / (t-t1); }' <(grep 'cpu ' /proc/stat) <(sleep 1;grep 'cpu ' /proc/stat) | sed 's/\.[0-9]*//')"

if [ "$CPU" -gt 65 ];then
    printf " %s%%" "$CPU"
fi

