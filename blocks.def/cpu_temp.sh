#!/bin/sh

ICONn="T " # icon for normal temperatures
ICONc="T " # icon for critical temperatures

crit=70 # critical temperature

read -r temp </sys/class/thermal/thermal_zone0/temp
temp="${temp%???}"
let temp=$temp+273

if [ "$temp" -lt "$crit" ] ; then
    printf "$ICONn%sK" "$temp"
else
    printf "$ICONc%sK" "$temp"
fi
