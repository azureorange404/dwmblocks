#!/bin/sh

script="$(basename "$0")"
path="$(realpath "$0" | sed -e "s/blocks[.def]*\/$script//")"
signal="$(grep "$script" "$path"config.def.h | sed 's/ \+/ /g' | cut -d' ' -f 6 | cut -d'}' -f1)"

case "$1" in
    1)  sigdwmblocks "$signal" 1
        sleep 5
        sigdwmblocks "$signal" 0;;
    3)  sigdwmblocks "$signal" 0;;
esac
