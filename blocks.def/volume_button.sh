#!/bin/sh

script="$(basename "$0")"
path="$(realpath "$0" | sed -e "s/blocks[.def]*\/$script//")"
signal="$(grep "$script" "$path"config.def.h | sed 's/ \+/ /g' | cut -d' ' -f 6 | cut -d'}' -f1)"

case "$1" in
    1) 
        pactl set-sink-mute @DEFAULT_SINK@ toggle
        sigdwmblocks "$signal"
        ;;
    3) 
        pactl set-sink-volume @DEFAULT_SINK@ 50%
        sigdwmblocks "$signal"
        ;;
esac
